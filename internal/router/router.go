package router

import (
	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/cache_redis/docs"
	"gitlab/cache_redis/internal/infrastructure/component"
	"gitlab/cache_redis/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	geo := controllers.Geo

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	r.Group(func(r chi.Router) {
		r.Route("/api/address", func(r chi.Router) {

			r.Post("/search", geo.SearchResponding)

		})
	})

	return r
}
