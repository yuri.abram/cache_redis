package modules

import (
	"gitlab/cache_redis/internal/infrastructure/component"
	gcontroller "gitlab/cache_redis/internal/modules/geo/controller"
)

type Controllers struct {
	Geo gcontroller.GeoController
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	return &Controllers{
		Geo: gcontroller.NewGeoCtl(services.GeoService, components),
	}
}
