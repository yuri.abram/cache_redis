package modules

import (
	"gitlab/cache_redis/internal/db/adapter"
	"gitlab/cache_redis/internal/infrastructure/cache"
	gstorage "gitlab/cache_redis/internal/modules/geo/storage"
)

type Storages struct {
	Geo gstorage.GeoStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Geo: gstorage.NewGeoStorage(sqlAdapter, cache),
	}
}
