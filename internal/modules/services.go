package modules

import (
	"gitlab/cache_redis/internal/infrastructure/component"
	gserv "gitlab/cache_redis/internal/modules/geo/service"
)

type Services struct {
	GeoService gserv.GeoServicer
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		GeoService: gserv.NewGeoService(components.Logger, storage.Geo),
	}
}
