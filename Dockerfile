FROM golang:1.20-alpine AS builder

WORKDIR /app

COPY ["./", "./"]

RUN go mod download


RUN go build -o ./bin/cmd ./cmd

FROM alpine

COPY ./.env ./.env
COPY --from=builder /app/bin/cmd ./

EXPOSE 8080

CMD ["/cmd"]